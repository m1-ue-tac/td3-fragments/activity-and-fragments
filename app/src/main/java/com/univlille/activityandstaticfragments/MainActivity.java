package com.univlille.activityandstaticfragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Exemple d'activité avec deux fragments statiques dont l'affchage se fait en cliquant sur le
 * bouton associé.
 * Ici, on a choisi de faire des show/hide plutôt que des replace ou remove, mais cela aurait tout
 * à fait été possible également.
 * Note : un fragment affiché derrière un autre fragment (donc caché) est consdéré comme visible !!! c-a-d que
 * isVisible() renverra vrai !
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // affichage du fragment au clic sur le bouton Fragment One
        findViewById(R.id.showFragmentOne).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // on récupère le gestionnaire de fragments
                FragmentManager fragmentManager = getSupportFragmentManager();

                // on commence une opération (add, remove, replace,...) sur les fragments
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                // on cherche à savoir s'il y a déjà un fragment avec le 'tag' F1.
                // Si non, on crée le fragment 1, et on affiche le fragment 1.
                // Si oui, on ne fait rien.
                Fragment fragment1 = fragmentManager.findFragmentByTag("F1");
                if (fragment1 == null) {
                    // Si ce fragment F1 n'existe pas, alors on commence par le créer
                    FragmentOne fragmentOne = FragmentOne.newInstance();

                    // on demande à la transaction de l'ajouter avec le tag 'F1'
                    fragmentTransaction.add(R.id.lesFragments, fragmentOne, "F1");
                    // si on garde le addToBackStack() ci-dessous, le fragment est "mémorisé" dans la pile;
                    // et donc peut être retrouvé après un 'back' plus tard.
                    //          fragmentTransaction.addToBackStack(null);
                    // Dans ce cas, quand on supprimera le fragment avec un remove, il faudra aussi
                    // appeler fragmentManager.popBackStack();  pour réellement supprimer le
                    // fragment, sinon il n'est pas détruit !
                } else {
                    // Pour information, le fragment 1 est ici à l'écran, pas forcément visible, mais dans l'état RESUMED. Pour le savoir :
                    // Toast.makeText(MainActivity.this, fragment1.getLifecycle().getCurrentState().toString(), Toast.LENGTH_SHORT).show();
                    fragmentTransaction.show(fragment1);
                }

                // on cherche à savoir s'il y a déjà un fragment avec le 'tag' F2 à l'écran
                Fragment fragment2 = fragmentManager.findFragmentByTag("F2");
                if (fragment2 != null) {
                    // on demande à la transaction de cacher ce fragment 2
                    fragmentTransaction.hide(fragment2);
                }

                // on exécute la transaction (qui pourrait contenir plusieurs ordres en même temps)
                fragmentTransaction.commit();
            }
        });

        // affichage du fragment au clic sur le bouton Fragment Two
        findViewById(R.id.showFragmentTwo).

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // on récupère le gestionnaire de fragments
                        FragmentManager fragmentManager = getSupportFragmentManager();

                        // on commence une opération (add, remove, replace,...) sur les fragments
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        // on cherche à savoir s'il y a déjà un fragment avec le 'tag' F2.
                        // Si non, on crée le fragment 2, et on affiche le fragment 2.
                        // Si oui, on ne fait rien.
                        Fragment fragment2 = fragmentManager.findFragmentByTag("F2");
                        if (fragment2 == null) {
                            // Si ce fragment F2 n'existe pas, alors on commence par le créer
                            FragmentTwo fragmentTwo = FragmentTwo.newInstance();

                            // on demande à la transaction de l'ajouter avec le tag 'F2'
                            fragmentTransaction.add(R.id.lesFragments, fragmentTwo, "F2");
                            // si on garde le addToBackStack() ci-dessous, le fragment est "mémorisé" dans la pile;
                            // et donc peut être retrouvé après un 'back' plus tard.
                            //          fragmentTransaction.addToBackStack(null);
                            // Dans ce cas, quand on supprimera le fragment avec un remove, il faudra aussi
                            // appeler fragmentManager.popBackStack();  pour réellement supprimer le
                            // fragment, sinon il n'est pas détruit !
                        } else fragmentTransaction.show(fragment2);

                        // on cherche à savoir s'il y a déjà un fragment avec le 'tag' F1 à l'écran
                        Fragment fragment1 = fragmentManager.findFragmentByTag("F1");
                        if (fragment1 != null) {
                            // on demande à la transaction de cacher ce fragment 2
                            fragmentTransaction.hide(fragment1);
                        }

                        // on exécute la transaction (qui pourrait contenir plusieurs ordres en même temps)
                        fragmentTransaction.commit();
                    }

                });
    }
}