package com.univlille.activityandstaticfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

/**
 * Un fragment tout simple...
 */

public class FragmentOne extends Fragment {

    public FragmentOne() {
    }

    public static FragmentOne newInstance() {
        FragmentOne fragment = new FragmentOne();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this.getContext(), "F1 on create", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toast.makeText(this.getContext(), "F1 on create view", Toast.LENGTH_SHORT).show();
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        Toast.makeText(this.getContext(), "F1 on pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        Toast.makeText(this.getContext(), "F1 on stop", Toast.LENGTH_SHORT).show();
    }
}