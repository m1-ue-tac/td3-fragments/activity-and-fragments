package com.univlille.activityandstaticfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

/**
 * Un autre fragment tout simple...
 */

public class FragmentTwo extends Fragment {

    public FragmentTwo() {
    }

    public static FragmentTwo newInstance() {
        FragmentTwo fragment = new FragmentTwo();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this.getContext(), "F2 on create", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toast.makeText(this.getContext(), "F2 on create view", Toast.LENGTH_SHORT).show();
        return inflater.inflate(R.layout.fragment_two, container, false);
    }

    public void onPause() {
        super.onPause();
        Toast.makeText(this.getContext(), "F2 on pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        Toast.makeText(this.getContext(), "F2 on stop", Toast.LENGTH_SHORT).show();
    }
}